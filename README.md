# Brave Viper

A project to learn how to better design programs using a purely functional approach.

## Run

~~~{.bash}
./sbt run

curl localhost:1776/internal/version
~~~

## Running code coverage

#### enable coverage

~~~{.bash}
sbt clean coverage test it:test
~~~

#### coverage report

~~~{.bash}
sbt coverageReport
~~~

The report will be in ```target/scala-2.12/scoverage-report```

