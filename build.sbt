lazy val commonSettings = Seq(
  name := "serviceSkel",
  organization := "jrigney",
  version := "0.1.0",
  scalaVersion := "2.12.2",
  shellPrompt := { s => Project.extract(s).currentProject.id + " > " },
  testOptions in Test += Tests.Argument("-oI"),
  scalacOptions in Compile ++= Seq(
                          "-deprecation",
                          "-encoding", "UTF-8",       // yes, this is 2 args
                          "-feature",
                          "-language:existentials",
                          "-language:higherKinds",
                          "-language:implicitConversions",
                          "-language:postfixOps",
                          "-language:reflectiveCalls",
                          "-unchecked",
                          "-Xfatal-warnings",
                          "-Xfuture",
                          "-Xlint",
                          "-Yno-adapted-args",
                          "-Ywarn-dead-code",        // N.B. doesn't work well with the ??? hole
                          "-Ywarn-numeric-widen",
                          "-Ywarn-unused-import",     // 2.11 only
                          "-Ywarn-value-discard",
                          "-target:jvm-1.8")
)


val test = Seq(
"org.scalactic" %% "scalactic" % "3.0.1",
"org.scalatest" %% "scalatest" % "3.0.1" % "test",
"org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
"junit" % "junit" % "4.12" % "test",
"org.hamcrest" % "hamcrest-all" % "1.3" % "test",
"org.typelevel" %% "scalaz-scalatest" % "1.1.2" % "test"
)

val logging = Seq(
"org.slf4j" % "slf4j-api" % "1.7.7",
"org.clapper" %% "grizzled-slf4j" % "1.3.1",
"ch.qos.logback" % "logback-classic" % "1.1.2"
)

val scalaz = Seq(
  "org.scalaz" %% "scalaz-core" % "7.2.6",
  "org.scalaz" %% "scalaz-concurrent" % "7.2.6"
)

val http4sVersion = "0.15.3a"
val http4s = Seq(
  "org.http4s" %% "http4s-core" % http4sVersion,
  "org.http4s" %% "http4s-argonaut" % http4sVersion,
  "org.http4s" %% "http4s-client" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "io.circe" %% "circe-generic" % "0.6.1", // Optional for auto-derivation of JSON codecs
  "io.circe" %% "circe-literal" % "0.6.1" // Optional for string interpolation to JSON model
)

lazy val root = (project in file(".")).
  configs( IntegrationTest ).
  settings( Defaults.itSettings : _*).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= test ++ logging ++ http4s
  )
