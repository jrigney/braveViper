package jrigney

import org.scalatest.{ Matchers, WordSpec }
import org.http4s._
import org.http4s.dsl._
import org.http4s.util.{ ByteVectorWriter, CaseInsensitiveString }
import org.typelevel.scalatest.TaskValues._

import scalaz.concurrent.Task
import scalaz.stream.Process

class RoutesSpec extends WordSpec with Matchers {

  "route" when {

    "uri is /internal/version" should {
      val request = Request(Method.GET, uri("/internal/version"))
      val actual = Routes.internalRoutes(request).syncValue

      "have a header Content-Type of application/json" in {
        actual.headers.get(CaseInsensitiveString("content-type")) should contain(Header("Content-Type", "application/json"))
      }
      "have a status code of 200" in {
        actual.status shouldBe Status.Ok
      }
      "have a body of 'version'" in {
        val byteVectors = actual.body.runLog.syncValue
        byteVectors.foreach { bytes =>
          val y = bytes.decodeString(java.nio.charset.Charset.forName("UTF-8"))
          val z = y.fold((_) => "exception", (right) => right)
          z shouldBe """{"version":"version"}"""
        }
      }
    }

    "uri is echo with body of 'hello bob'" should {

      val bvWriter = ByteVectorWriter()
      val bob = bvWriter << "hello bob"
      val body = Process(bob.toByteVector).toSource
      val request = Request(Method.POST, uri("/internal/echo"), body = body)
      val actual = Routes.internalRoutes(request).syncValue
      "have a header Content-Type of text/plain" in {
        actual.headers.get(CaseInsensitiveString("content-type")) should contain(Header("Content-Type", "text/plain;  charset=UTF-8"))
      }
      "have a status code of 200" in {
        actual.status shouldBe Status.Ok
      }
      "have a body of 'hello bob'" in {
        val bodyTask = actual.as[String].flatMap { x => Task.apply(x) }
        val body = bodyTask.unsafePerformSync
        body shouldBe """hello bob"""
      }
    }

  }
}
