package jrigney.internal

import org.scalatest.{ Matchers, WordSpec }

import scalaz.concurrent.Task
import org.typelevel.scalatest.TaskValues._

class InternalServiceSpec extends WordSpec with Matchers {

  "A Set" when {
    "empty" should {
      "have size 0" in {
        assert(Set.empty.size == 0)
      }

      "produce NoSuchElementException when head is invoked" in {
        assertThrows[NoSuchElementException] {
          Set.empty.head
        }
      }
    }
  }

  "getInternalVersion" should {
    "return version" in {
      val r: Task[ApplicationVersion] = getInternalVersion(() => Task.now(ApplicationVersion("version")))
      r.syncValue shouldBe ApplicationVersion("version")
    }
  }
}
