package jrigney

import io.circe.syntax._
import jrigney.internal.{ ApplicationVersion, getInternalVersion }
import org.http4s.{ HttpService }
import org.http4s.dsl._
import io.circe.generic.auto._
import org.http4s.circe._
import org.http4s.server.syntax._

import scalaz.concurrent.Task

object Routes {
  val helloRoute = HttpService {
    case GET -> Root / "hello" / name => Ok(s"Hello, $name.")
  }

  val internalRoutes = HttpService {
    case request @ GET -> Root / "internal" / "version" =>
      getInternalVersion(() => Task.now(ApplicationVersion("version"))).
        flatMap(x => Ok(x.asJson))
    case request @ POST -> Root / "internal" / "echo" =>
      //.as takes an implicit EntityDecoder.
      // EntityDecoder is a typeclass so you need to implement an EntityDecoder[A]
      // fortunately there are Presupplied Encoders/Decoders in http4s
      request.as[String].flatMap(Ok(_))
  }

  val rootRoutes = helloRoute orElse internalRoutes

}
