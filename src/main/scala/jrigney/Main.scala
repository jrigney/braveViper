package jrigney

import jrigney.tweet.TweetService
import org.http4s.server.{ Server, ServerApp }
import org.http4s.server.blaze._

import scalaz.concurrent.Task

object Main extends ServerApp {

  val apiRoutes = TweetService.routes

  override def server(args: List[String]): Task[Server] = {
    BlazeBuilder
      .bindHttp(1776, "localhost")
      .mountService(Routes.rootRoutes, "/")
      .mountService(apiRoutes, "/api")
      .start
  }
}
