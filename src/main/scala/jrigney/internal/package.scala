package jrigney

import scalaz.concurrent.Task

package object internal {
  def getInternalVersion(fn: () => Task[ApplicationVersion]): Task[ApplicationVersion] =
    fn()
}
