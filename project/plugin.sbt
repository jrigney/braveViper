addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.6")
addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.1.1")
addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.7.1")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")
